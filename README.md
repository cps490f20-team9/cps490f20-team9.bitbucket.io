University of Dayton

Department of Computer Science

CPS 490 - Fall, 2020

Dr. Phu Phung


## Capstone II Proposal


# Project Topic

Optical Character Recognition


# Team members
1.  Jacob Blair <Blairj9@udayton.edu>
2.  Caleb Hoskins <hoskinsc1@udayton.edu>
3.  Muhammad Ndao <ndaom1@udayton.edu>
4.  Marcus Chronabery<mchronabery1@udayton.edu>


# Company Mentors

Hung Nguyen <hung.nguyen@novobi.com> 

Novobi

Company address: 8920 Business Park Dr #250, Austin, TX 78759

## Project homepage

[Team 9 Project Homepage](https://cps490f20-team9.bitbucket.io/)


# Overview

The overall goal of this project is to upload a PDF or other image document with the expected template format, parse the contents using an Optical Character Recognition engine, and present the results to the user for verification before storing in a database.

![ocr_architecture.png](./images/ocr_architecture.png)

Figure 1. - A Sample of Overview Architecture of the proposed project.

# Project Context and Scope

The Minimum Viable Product is the ability to upload a document to our service, parse it's contents using an OCR engine, and present it to the user before storing in a database.

![Overview Diagram](images/overview_diagram.jpg)

Figure 2. - An Overview Diagram of what a user can accomplish with our application.


# High-level Requirements

* Upload PDF/Image
* Parse PDF/Image
* Present Results To User
* Store Results In Database


# Optional Features
* User Accounts
* Edit Certain Fields Prior to Saving
* Display Previously Saved Documents


# Technology

* CSS3
* HTML5
* Node.js
* MongoDB
* Deployed using Heroku
* Integration with Amazon/Google Optical Character Recognition engine


# Project Management
[Trello Board](https://trello.com/b/HT6GxvGu/opticalcharacterrecognition)

[Bitbucket Repository](https://bitbucket.org/cps490f20-team9/cps490f20-team9.bitbucket.io/src/master/)

![Spring 2021 Timeline](images/trello_board.png)

![gantt_chart.png](/images/gantt_chart.png) 



# Company Support

Hung is our point of contact with any requirement questions. He didn't have any specific meeting schedule in mind, just wanted occasional demonstrations of functionality.